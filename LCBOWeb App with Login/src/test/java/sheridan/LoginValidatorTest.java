package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegularCharsAndNumberOnly( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "deep1310" ) );
	}
	
	@Test
	public void testIsValidLoginRegularAtLeastSixChars( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "deepshah" ) );
	}
	
	
	@Test
	public void testIsValidLoginExceptionCharsAndNumbersOnly( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("deep@!^") );
	}
	
	@Test
	public void testIsValidLoginExceptionAtLeastSixChars( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName(null) );
	}
	
	
	@Test
	public void testIsValidLoginBoundaryInCharsAndNumbersOnly( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("deep1310") );
	}
	
	@Test
	public void testIsValidLoginBoundaryInAtLeastSixChars( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("deepsd") );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutCharsAndNumbersOnly( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("deep13!") );
	}
	
	@Test
	public void testIsValidLoginBoundaryOutAtLeastSixChars( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("deepd") );
	}

}
