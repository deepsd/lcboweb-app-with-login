package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		String loginNameRegex = "[A-Za-z0-9]{6,}$";
        Pattern pat = Pattern.compile(loginNameRegex);
        
        if(loginName == null) {
        	return false;
        }

		return  pat.matcher(loginName).matches();
	}
}
